# Imports
# !/usr/bin/env python
from troposphere import Base64, FindInMap, GetAtt, GetAZs, Join, Output, Parameter, Ref, Select, Tags, Template
import troposphere.ec2 as ec2
import troposphere.rds as rds
from troposphere import Export
from troposphere import *

template = Template()

template.add_version("2010-09-09")

template.add_description("Creates network layer infrastructure. Includes: VPC, 2 public subnets, 2 private subnets, 1 internet gateway")

# ###################################################
# ################# Mappings ########################
# ###################################################

region_to_az_mapping = template.add_mapping('RegionToAZMapping', {
    'ap-northeast-1': {
        "AZ": ["ap-northeast-1a", "ap-northeast-1b", "ap-northeast-1c"]
    },
    'ap-southeast-1': {
        "AZ": ["ap-southeast-1a", "ap-southeast-1b"]
    },
    'ap-southeast-2': {
        "AZ": ["ap-southeast-2a", "ap-southeast-2b"]
    },
    'eu-central-1': {
        "AZ": ["eu-central-1a", "eu-central-1b"]
    },
    'eu-west-1': {
        "AZ": ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    },
    'sa-east-1': {
        "AZ": ["sa-east-1a", "sa-east-1b"]
    },
    'us-east-1': {
        "AZ": ["us-east-1a", "us-east-1b", "us-east-1c",
               "us-east-1d", "us-east-1e"]
    }
})

# ###################################################
# ################# Parameters ######################
# ###################################################

vpc_cidr_param = template.add_parameter(Parameter(
    "VpcId",
    Description="Please input the CIDR range of the VPC",
    Type="String",
    AllowedPattern="([0-9]{1,3}\.){3}[0-9]{1,3}($|/(16|24))",
    ConstraintDescription="CIDR range error: please enter a valid CIDR range."
))

az_param_1 = template.add_parameter(Parameter(
    "AZ1",
    Description="Please select the AZs you would like to use",
    Type="AWS::EC2::AvailabilityZone::Name"
))

az_param_2 = template.add_parameter(Parameter(
    "AZ2",
    Description="Please select the AZs you would like to use",
    Type="AWS::EC2::AvailabilityZone::Name"
))

private_subnet_1_cidr_param = template.add_parameter(Parameter(
    "PrivateSubnetId1",
    Description="Please input the CIDR range of the first private subnet",
    Type="String",
    AllowedPattern="([0-9]{1,3}\.){3}[0-9]{1,3}($|/(16|24))",
    ConstraintDescription="CIDR range error: please enter a valid CIDR range."
))

private_subnet_2_cidr_param = template.add_parameter(Parameter(
    "PrivateSubnetId2",
    Description="Please input the CIDR range of the second private subnet",
    Type="String",
    AllowedPattern="([0-9]{1,3}\.){3}[0-9]{1,3}($|/(16|24))",
    ConstraintDescription="CIDR range error: please enter a valid CIDR range."
))

public_subnet_1_cidr_param = template.add_parameter(Parameter(
    "PublicSubnetId1",
    Description="Please input the CIDR range of the first public subnet",
    Type="String",
    AllowedPattern="([0-9]{1,3}\.){3}[0-9]{1,3}($|/(16|24))",
    ConstraintDescription="CIDR range error: please enter a valid CIDR range."
))

public_subnet_2_cidr_param = template.add_parameter(Parameter(
    "PublicSubnetId2",
    Description="Please input the CIDR range of the second public subnet",
    Type="String",
    AllowedPattern="([0-9]{1,3}\.){3}[0-9]{1,3}($|/(16|24))",
    ConstraintDescription="CIDR range error: please enter a valid CIDR range."
))

# ###################################################
# ################# Conditionals ####################
# ###################################################

# ###################################################
# ################# Resources # #####################
# ###################################################

ref_stack_name = Ref('AWS::StackName')

# ############### General VPC resources #############

vpc = template.add_resource(
    ec2.VPC(
        'VPC',
        CidrBlock=Ref(vpc_cidr_param),
        Tags=Tags(Name="Tristan-VPC", Owner="tristan.metcalfe@cloudreach.com")
    )
)

internet_gateway = template.add_resource(
    ec2.InternetGateway(
        'InternetGateway',
        Tags=Tags(
            Name="Tristan Internet Gate",
            Owner="tristan.metcalfe@cloudreach.com")
            )
        )

gateway_attachment = template.add_resource(
    ec2.VPCGatewayAttachment(
        'AttachGateway',
        VpcId=Ref(vpc),
        InternetGatewayId=Ref(internet_gateway)
        )
    )

# ########### Private subnets ####################

private_subnet_1 = template.add_resource(
    ec2.Subnet(
        'PrivateSubnet1',
        AvailabilityZone=Ref(az_param_1),
        CidrBlock=Ref(private_subnet_1_cidr_param),
        VpcId=Ref(vpc),
        Tags=Tags(
            Name="Private subnet 1",
            Owner="tristan.metcalfe@cloudreach.com"))
    )

private_subnet_2 = template.add_resource(
    ec2.Subnet(
        'PrivateSubnet2',
        AvailabilityZone=Ref(az_param_2),
        CidrBlock=Ref(private_subnet_2_cidr_param),
        VpcId=Ref(vpc),
        Tags=Tags(
            Name="Private subnet 2",
            Owner="tristan.metcalfe@cloudreach.com"))
    )

public_subnet_1 = template.add_resource(
    ec2.Subnet(
        'PublicSubnet1',
        AvailabilityZone=Ref(az_param_1),
        CidrBlock=Ref(public_subnet_1_cidr_param),
        VpcId=Ref(vpc),
        Tags=Tags(
            Name="Public subnet 1",
            Owner="tristan.metcalfe@cloudreach.com"))
    )

public_subnet_2 = template.add_resource(
    ec2.Subnet(
        'PublicSubnet2',
        AvailabilityZone=Ref(az_param_2),
        CidrBlock=Ref(public_subnet_2_cidr_param),
        VpcId=Ref(vpc),
        Tags=Tags(
            Name="Public subnet 2",
            Owner="tristan.metcalfe@cloudreach.com"))
    )

db_subnet = template.add_resource(
    rds.DBSubnetGroup(
        "DBSubnetGroup",
        DBSubnetGroupDescription=Base64("Subnet for DB"),
        SubnetIds=[
            Ref(private_subnet_1),
            Ref(private_subnet_2)
        ],
        Tags=Tags(
            Name="DB subnet",
            Owner="tristan.metcalfe@cloudreach.com"
        )
    )
)

# ################### Route Tables ##########################

public_route_table = template.add_resource(
    ec2.RouteTable(
        "PublicRouteTable",
        VpcId=Ref(vpc),
        Tags=Tags(
            Name="Tristan - Public route table",
            Owner="tristan.metcalfe.@cloudreach.com"
        )
    )
)

public_route = template.add_resource(
    ec2.Route(
        "PublicRoute",
        DestinationCidrBlock="0.0.0.0/0",
        GatewayId=Ref(internet_gateway),
        RouteTableId=Ref(public_route_table)
    )
)


def associate_subnets_routetable(subnets, route_table, subnetType):
    subnet_assoc = []
    counter = 1
    for subnet in subnets:
        subnet_assoc.append(
            template.add_resource(
                ec2.SubnetRouteTableAssociation(
                    subnetType + "SubnetAssoc" + str(counter),
                    RouteTableId=Ref(route_table),
                    SubnetId=Ref(subnet)
                )
            )
        )
        counter = counter + 1
    return subnet_assoc

public_subnets = [public_subnet_1, public_subnet_2]

public_subnet_assocs = associate_subnets_routetable(public_subnets, public_route_table, "Public")

# ###################################################
# ################# Outputs #########################
# ###################################################

public_subnet_1_output = template.add_output(Output(
        "PublicSubnet1Output",
        Description="Output of first public subnet",
        Value=Ref(public_subnet_1),
        Export=Export(Join("", [ref_stack_name, "PublicSubnet1"]))
    ))

public_subnet_2_output = template.add_output(Output(
        "PublicSubnet2Output",
        Description="Output of second public subnet",
        Value=Ref(public_subnet_2),
        Export=Export(Join("", [ref_stack_name, "PublicSubnet2"]))
    ))

private_subnet_1_output = template.add_output(Output(
        "PrivateSubnet1Output",
        Description="Output of first private subnet",
        Value=Ref(private_subnet_1),
        Export=Export(Join("", [ref_stack_name, "PrivateSubnet1"]))
    ))

private_subnet_2_output = template.add_output(Output(
        "PrivateSubnet2Output",
        Description="Output of second private subnet",
        Value=Ref(private_subnet_2),
        Export=Export(Join("", [ref_stack_name, "PrivateSubnet2"]))
    ))

db_subnet_output = template.add_output(Output(
        "DBSubnetOutput",
        Description="Output of DB subnet",
        Value=Ref(db_subnet),
        Export=Export(Join("", [ref_stack_name, "DBSubnetGroup"]))
))


print (template.to_json())
