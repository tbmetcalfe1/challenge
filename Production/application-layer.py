#!/usr/bin/env python
from troposphere import Base64, FindInMap, GetAtt, GetAZs, Join, Output, Parameter, Ref, Select, Tags, Template
import troposphere.ec2 as ec2
import troposphere.elasticloadbalancing as elb
import troposphere.route53 as r53
import troposphere.autoscaling as asg
import troposphere.rds as rds
import troposphere.policies as pol
from troposphere import Export
from troposphere import ImportValue
from troposphere.ec2 import *

template = Template()

template.add_version("2010-09-09")

template.add_description("Creates application layer components. Includes: autoscaling group, elastic load balancer, database instance")

# ###################################################
# ################# Parameters ######################
# ###################################################

vpc_param = template.add_parameter(Parameter(
    "VPCParam",
    Type="AWS::EC2::VPC::Id",
    Description="Choose VPC: "
))

hosted_zone_param = template.add_parameter(Parameter(
    "DNSParam",
    Type="AWS::Route53::HostedZone::Id",
    Description="Choose hosted zone ID"
))

hosted_zone_name_param = template.add_parameter(Parameter(
    "HZNameParam",
    Type="String",
    Description="Name of hosted zone"
))

elb_subnet_param = template.add_parameter(Parameter(
    "ELBSubnetParam",
    Type="AWS::EC2::Subnet::Id",
    Description="Choose public subnet: "
    )
)

jumphost_subnet_param = template.add_parameter(Parameter(
    "JumphostSubnetParam",
    Type="AWS::EC2::Subnet::Id",
    Description="Choose public subnet for jumphost: "
))

db_username_param = template.add_parameter(Parameter(
        "DBUsernameParam",
        Type="String",
        Description="Database username"
    )
)

db_password_param = template.add_parameter(Parameter(
        "DBPasswordParam",
        Type="String",
        Description="Database password"
    )
)

key_pair_param = template.add_parameter(Parameter(
    "KeyPairParam",
    Type="AWS::EC2::KeyPair::KeyName",
    Description="Choose key name: "
    )
)

ami_id_param = template.add_parameter(Parameter(
        "AMIIDParam",
        Type="String",
        Description="AMI ID"
    )
)

# ###################################################
# ################# Mappings ########################
# ###################################################

region_to_ami_mapping = template.add_mapping(
    'RegionToAMIMapping', {
        'ap-northeast-1': {
            'AMI': ''
        },
        'ap-southeast-1': {
            'AMI': 'ami-1c2e887f'
        },
        'ap-southeast-2': {
            'AMI': ''
        },
        'eu-west-1': {
            'AMI': 'ami-d41d58a7'
        }
    }
    )

# ###################################################
# ################# Conditionals ####################
# ###################################################

# ###################################################
# ################# Outputs #########################
# ###################################################

# ###################################################
# ################# Resources # #####################
# ###################################################

# ################ Load Balancer ####################

elb_sg = template.add_resource(
    SecurityGroup(
        "elbsg",
        GroupDescription="Security group for ELB",
        SecurityGroupEgress=[],
        SecurityGroupIngress=[],
        Tags=Tags(
            Name="Tristan-ELB-SG",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        VpcId=Ref(vpc_param)
    )
)

elb_sg_ingress = template.add_resource(
    SecurityGroupIngress(
        "ELBSGIngress",
        CidrIp="0.0.0.0/0",
        FromPort="80",
        GroupId=Ref(elb_sg),
        IpProtocol="tcp",
        ToPort="80"
    )
)

load_balancer = template.add_resource(
    elb.LoadBalancer(
        "ElasticLoadBalancer",
        CrossZone=True,
        LoadBalancerName="Tristan-LoadBalancer",
        Listeners=[
            elb.Listener(
                InstancePort=80,
                InstanceProtocol="HTTP",
                LoadBalancerPort=80,
                Protocol="HTTP"
            )
        ],
        Subnets=[
            ImportValue("TristanPublicSubnet1"),
            ImportValue("TristanPublicSubnet2")
        ],
        Tags=Tags(
            Name="Tristan-ElasticLoadBalancer",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        SecurityGroups=[
            Ref(elb_sg)
        ]
    )
)

# ######################### Route 53 / DNS ###################
"""
hosted_zone = template.add_resource(
    r53.HostedZone(
        "HostedZone",
        Name=Ref(hosted_zone_name_param),
        VPCs=[
            r53.HostedZoneVPCs(
                VPCId=Ref(vpc_param),
                VPCRegion="ap-southeast-1"
            )
        ]
    )
)
"""

"""alias_target = template.add_resource(
    r53.AliasTarget(
        DNSName=GetAtt(Ref(load_balancer), "DNSName"),
        HostedZoneId=Ref(hosted_zone_param),
        EvaluateTargetHealth=True
    )
)"""

"""
record_set = template.add_resource(
    r53.RecordSetType(
        "RecordSet",
        DependsOn=[
            "ElasticLoadBalancer"
            ],
        AliasTarget=r53.AliasTarget(
            DNSName=GetAtt("ElasticLoadBalancer", "DNSName"),
            HostedZoneId=Ref(hosted_zone_param),
            EvaluateTargetHealth=True
        ),
        Comment="Production environment",
        HostedZoneId=Ref(hosted_zone),
        Name=Ref(hosted_zone_name_param),
        Region="ap-southeast-1",
        Type="A"
    )
)
"""

# ######################### Jump host ########################

jumphost_sg = template.add_resource(
    SecurityGroup(
        "JumphostSG",
        GroupDescription="Security group for jumphost",
        SecurityGroupEgress=[],
        SecurityGroupIngress=[],
        Tags=Tags(
            Name="Tristan-Jumphost-SG",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        VpcId=Ref(vpc_param)
    )
)

jumphost_ingress_ssh = template.add_resource(
    SecurityGroupIngress(
        "JumphostSGIngressSSH",
        CidrIp="0.0.0.0/0",
        FromPort="22",
        GroupId=Ref(jumphost_sg),
        IpProtocol="tcp",
        ToPort="22"
    )
)

jumphost = template.add_resource(
    ec2.Instance(
        "Jumphost",
        AvailabilityZone="",
        EbsOptimized=False,
        ImageId="ami-b953f2da",
        InstanceType="t2.micro",
        KeyName=Ref(key_pair_param),
        NetworkInterfaces=[
            NetworkInterfaceProperty(
                AssociatePublicIpAddress=True,
                DeviceIndex="0",
                GroupSet=[Ref(jumphost_sg)],
                SubnetId=Ref(jumphost_subnet_param)
            )
            ],
        Tags=Tags(Name="Tristan-Jumphost"),

    )
)

# ############### Autoscaling group components ###############

launch_config_sg = template.add_resource(
    SecurityGroup(
        "LaunchConfigSG",
        GroupDescription="Security group for autoscaling",
        SecurityGroupEgress=[],
        SecurityGroupIngress=[],
        Tags=Tags(
            Name="Tristan-Private-Autoscaling-SG",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        VpcId=Ref(vpc_param)
    )
)

lc_ingress_http = template.add_resource(
    SecurityGroupIngress(
        "IngressHTTP",
        FromPort="80",
        GroupId=Ref(launch_config_sg),
        IpProtocol="tcp",
        SourceSecurityGroupId=Ref(elb_sg),
        ToPort="80"
    )
)

lc_ingress_ssh = template.add_resource(
    SecurityGroupIngress(
        "PrivateInstanceSGIngressSSH",
        FromPort="22",
        GroupId=Ref(launch_config_sg),
        IpProtocol="tcp",
        SourceSecurityGroupId=Ref(jumphost_sg),
        ToPort="22"
    )
)

asg_launch_config = template.add_resource(
    asg.LaunchConfiguration(
        "ASGLaunchConfig",
        AssociatePublicIpAddress=True,
        EbsOptimized=False,
        ImageId=Ref(ami_id_param),
        InstanceType="t2.micro",
        KeyName=Ref(key_pair_param)
    )
)

# ################ Autoscaling group ###############
autoscaling_group = template.add_resource(
    asg.AutoScalingGroup(
        "AutoscalingGroup",
        # AvailabilityZones=["eu-west-1a", "eu-west-1b", "eu-west-1c"],
        LaunchConfigurationName=Ref(asg_launch_config),
        LoadBalancerNames=[Ref(load_balancer)],
        MaxSize="5",
        MinSize="2",
        Tags=asg.Tags(
            Name=("Tristan-Autoscaling group", True)
        ),
        UpdatePolicy=pol.UpdatePolicy(
            AutoScalingRollingUpdate=pol.AutoScalingRollingUpdate(
                MinInstancesInService="1"
            )
        ),
        VPCZoneIdentifier=[Ref(elb_subnet_param)]
    )
)

# ################ Database Tier ####################

ec2_db_sg = template.add_resource(
    SecurityGroup(
        "ec2DBsg",
        GroupDescription="Security group for DB",
        SecurityGroupEgress=[],
        SecurityGroupIngress=[],
        Tags=Tags(
            Name="Tristan-DB-SG",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        VpcId=Ref(vpc_param)
    )
)

ec2_db_ingress = template.add_resource(
    SecurityGroupIngress(
        "EC2DBSGIngress",
        FromPort="1433",
        GroupId=Ref(ec2_db_sg),
        IpProtocol="tcp",
        SourceSecurityGroupId=Ref(launch_config_sg),
        ToPort="1433"
    )
)

db_sg = template.add_resource(
    rds.DBSecurityGroup(
        "dbsg",
        GroupDescription="Security group for DB",
        DBSecurityGroupIngress=[],
        Tags=Tags(
            Name="Tristan-DB-SG",
            Owner="tristan.metcalfe@cloudreach.com"
        ),
        EC2VpcId=Ref(vpc_param)
    )
)

db_sg_ingress = template.add_resource(
    rds.DBSecurityGroupIngress(
        "DBSGIngress",
        # CIDRIP=Base64(),
        DBSecurityGroupName=Ref(db_sg),
        EC2SecurityGroupId=Ref(ec2_db_sg)
    )
)

rds_instance = template.add_resource(
    rds.DBInstance(
        "DBMaster",
        AllocatedStorage="100",
        DBInstanceClass="db.t2.small",
        DBName="TristanDBMaster",
        DBSecurityGroups=[
            Ref(db_sg)
        ],
        # DBSubnetGroupName="Private subnet 1",
        DBSubnetGroupName=ImportValue("TristanDBSubnetGroup"),
        Engine="MySQL",
        EngineVersion="5.6.27",
        PubliclyAccessible=False,
        MasterUsername=Ref(db_username_param),
        MasterUserPassword=Ref(db_password_param),
        MultiAZ=True
    )
)

print(template.to_json())
